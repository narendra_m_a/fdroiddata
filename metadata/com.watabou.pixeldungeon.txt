Categories:Games
License:GPLv3+
Web Site:http://pixeldungeon.watabou.ru
Source Code:https://github.com/watabou/pixel-dungeon
Issue Tracker:https://github.com/watabou/pixel-dungeon/issues
Bitcoin:1LyLJAzxCfieivap1yK3iCpGoUmzAnjdyK

Auto Name:Pixel Dungeon
Summary:Rogue-like
Description:
Traditional roguelike game with pixel-art graphics and simple interface.
.

Repo Type:git
Repo:https://github.com/watabou/pixel-dungeon

Build:1.7.1c,59
    disable=inconvertible types errors - https://github.com/watabou/pixel-dungeon/pull/1
    commit=33c27cdb587878f771e999103ba186f190f9446c
    srclibs=PDClasses@4902d172b0fccede3acce833efe9b4d5d77b9c33
    prebuild=cp -R $$PDClasses$$/com/watabou/* src/com/watabou/ && \
        echo -e 'java.source=1.7\njava.target=1.7\njava.encoding=ISO-8859-1' >> ant.properties
    target=android-18

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.7.1c
Current Version Code:59

